#/home/david/spark-2.2.1-bin-hadoop2.7/bin/spark-submit finance_pyspark.py

#----------------------------LIBRERIE--------------------------------------
import numpy as np                  #per array e esponenziali
import random                       #per generare numero casuale
import time                         #per ottenere la data corrente
import matplotlib.pyplot as plt     #per i plot
#import urlopen
import string
import urllib.request as urllib2    #per web scraper

from pyspark import SparkContext


sc=SparkContext()                   #inizializza context per spark



#----------------------------FUNZIONI--------------------------------------

def diff(x):               #funzione che calcola di=(xi-x*_i)^2
  z=np.zeros(len(x))
  global x_star
  for i in range(0,len(x)):
        z[i]=(x[i] -x_star[i])**2
  return z

def convert(x):            #funzione che converte una string in double
  for i in range(0,len(x)):
    x[i]=float(x[i])
  return x

def new_x_star(x,g):        #funzione che dato vettore x=x1,x2,x3 e dato un numero g, crea un nuovo vettore x*=x2,x3,g
  z=np.zeros(len(x))
  z[len(x)-1]=g
  for i in range(0,len(x)-1):
    z[i]=x[i+1]
  return z



#----------------------------DATI MODIFICARE (INPUT)-------------------------
minimo=1                       #percentile piu basso
piccolo=25
mediana=50                       #percentile medio
grande=75
massimo=99                       #percentile piu alto
sim=5                       #quanti output può prendere, ad esempio con sim=3, prende i tre output dei vettori con le distanze piu piccole    e poi ne sceglie uno a caso
n=60                        #quanti minuti si vogliono predire, ad esempio n=15 vuol dire che predirà i successivi 15 minuti
m=100                        #indica quante simulazioni ai vogliono fare, ossia con n=15 e m=3 vuol dire che si prediranno 3 volte i successivi n minuti
r=30                        #lunghezza vettore input
tim=60                      #ogni quanti secondi prendere online il dato per creare file input

#----------------------------DATI NON MODIFICARE-------------------------
Z=np.zeros((6, n))          #matrice che ha nella prima riga i valori minimi per ogni simulazione in ogni n, nella seconda la media e nella terza il max.
K=np.zeros((m, n))          #matrice che ha nella prima riga i valori minimi per ogni simulazione in ogni n, nella seconda la media e nella terza il max.
b=np.zeros(r+n)             #array contenente i dati raccolti online
eurusd=np.zeros(r)          #vettore per usare da input
x_pred=np.zeros(r-1)        #vettore input dei logrendimenti
fileX="all_30log_volte_0__x.csv"
fileY="all_30log_volte_0__y.csv"
X = sc.textFile(fileX)
Y = sc.textFile(fileY)


for d in range(0,r+n):
 #print ('Raccolta dati: ')+str(d+1)+('/')+str(r+n)
 print(d+1)
 time.sleep(tim)
 res = urllib2.urlopen('http://webrates.truefx.com/rates/connect.html?f=html')
 time_str = res.read().strip()
 type(time_str)
 t1=time_str[53:57]
 t2=time_str[66:69]
 t=t1+t2
 t=float(t)
 b[d]=t
i=1
print(b)


eurusd=b[0:r]
a=b[30:n+r]



#calcola log rendimenti
for d in range(0,r-1):
  x_pred[d]=np.log(eurusd[i]/eurusd[i-1])                                 #ne calcola i logrendimenti
  i=i+1

ultimo=eurusd[r-1]      #prende ultimo valore come valore 
                        #per predire altri valori                                                       #prende come ultimo valore, quello del vettore eurusd, per usarlo per predire
print(ultimo)
print(x_pred)
print(a)


#sia per matrice X (input) che Y (output), splita ciascuna
#stringa e converti in float ciascun valore
X=X.map(lambda x:x.replace(',',' ').lower())
X=X.map(lambda x:x.split())
X=X.map(lambda x:convert(x))

Y=Y.map(lambda x: x.split())
Y=Y.map(lambda x:convert(x))

#aggiungi la chiave per ogni record di output
Y= Y.flatMap(lambda x:x)

Y = Y.zipWithIndex()
Y = Y.map(lambda q:(int(q[1]),q[0]))



for j in range(0,m): #"for" per ogni simulazione
 x_star=x_pred


 for i in range(0,n):  #"for" per ogni minuto di predizione
  print("SIMULAZIONE")
  print(j+1)
  print("su")
  print(m)

  print("sim interna")
  print(i+1)
  print("su")
  print(n)

  X2=X.map(lambda x:diff(x)) #calcola per ogni valore la
                             #differenza euclidea

  X2= X2.flatMap(lambda x:x) #aggiunge la key
  X2=X2.zipWithIndex()
  X2=X2.map(lambda q: (int(q[1]/len(x_pred)), q[0]))
  
  X2=X2.reduceByKey(lambda x,y:x+y) #riduce per la chiave
  X2=X2.sortBy(lambda x: x[1])      #ordina per la distanza
  X2=X2.map(lambda x:x[0])
  X2=X2.take(sim)                   #prende i primi "sim",
                                    #ossia i migliori

  #prende casualmente uno di questi "sim"
  t=time.clock()*648
  random.seed(t**9)
  r=random.uniform(0, len(X2)-1)
  
  r=X2[int(r)]


  x1=Y.lookup(r) #questo valore sarà utilizzato aggiungendolo
                 #all'array di input
  x_star=new_x_star(x_star,x1[0])
  K[j][i]=x1[0]  #lo si aggiunge anche alla una matrice output
                 #per questa simulazione



for t in range(0,m):
 K[t][0]=ultimo*np.exp(K[t][0])   #lo si converte da log rendimento
                                  #a rendimento normale
for h in range(1,n):
 for t in range(0,m):
  K[t][h]=K[t][h-1]*np.exp(K[t][h])

#si calcolano i vari percentili
for f in range(0,n):
 Z[0][f]= np.percentile(K.T[f], minimo)#mostra a schermo i plot
 Z[4][f]= np.percentile(K.T[f], piccolo)#mostra a schermo i plot
 Z[1][f]= np.percentile(K.T[f], mediana)#mostra a schermo i plo
 Z[5][f]= np.percentile(K.T[f], grande)#mostra a schermo i plot
 Z[2][f]= np.percentile(K.T[f], massimo)#mostra a schermo i plot



Z[3][:]=a


print(Z[0][:])
print(Z[1][:])
print(Z[2][:])
print(Z[3][:])
#----------------------------PLOT-------------------------
#lo si splita per valutare il modello
x=range(0,n)
fig, ax=plt.subplots()                                          #plot dei tre grafici, con blu il minimo , con nero la media e con rosso il massimo
ax.plot(x, Z[0][:],color='blue')
ax.plot(x, Z[4][:],color='yellow')
ax.plot(x, Z[1][:],color='green')
ax.plot(x, Z[5][:], color='orange')
ax.plot(x, Z[2][:], color='red')
ax.plot(x, Z[3][:], color='black')
plt.show(block=True)
                                                    #mostra a schermo i plot
